import { Controller, Get } from '@nestjs/common';
import {
  DNSHealthIndicator,
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
} from '@nestjs/terminus';

@Controller('healthz')
export class HealthController {
  constructor(private health: HealthCheckService, private dns: DNSHealthIndicator) {}

  @Get('liveness')
  async livenessCheck(): Promise<void> {
    return;
  }

  @Get('readiness')
  @HealthCheck()
  async readinessCheck(): Promise<HealthCheckResult> {
    return this.health.check([() => this.dns.pingCheck('nestjs-docs', 'https://docs.nestjs.com')]);
  }
}
