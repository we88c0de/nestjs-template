import { NestFastifyApplication } from '@nestjs/platform-fastify';
import helmet from 'fastify-helmet';
import { corsOptions, helmetOptions } from './constants';

export const configure = (app: NestFastifyApplication): void => {
  app.enableCors(corsOptions);

  app.register(helmet, helmetOptions);
};
