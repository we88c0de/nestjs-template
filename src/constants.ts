import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import * as helmet from 'helmet';

export const helmetOptions: Parameters<typeof helmet>[0] = {};

export const corsOptions: CorsOptions = {};
