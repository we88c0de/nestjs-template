import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(200).expect('Hello World!');
  });

  it('/healthz/liveness (GET)', () => {
    return request(app.getHttpServer()).get('/healthz/liveness').expect(200).expect('');
  });

  it('/healthz/readiness (GET)', () => {
    return request(app.getHttpServer())
      .get('/healthz/readiness')
      .expect(200)
      .expect({
        status: 'ok',
        info: {
          'nestjs-docs': {
            status: 'up',
          },
        },
        error: {},
        details: {
          'nestjs-docs': {
            status: 'up',
          },
        },
      });
  });
});
